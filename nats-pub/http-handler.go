package main

import (
	"io/ioutil"
	"log"
	"net/http"

	stan "github.com/nats-io/go-nats-streaming"
)

const (
	clusterID = "test-cluster"
	clientID  = "test-publisher"
)

func main() {
	sc, err := stan.Connect(
		clusterID,
		clientID,
		stan.NatsURL(stan.DefaultNatsURL),
	)
	if err != nil {
		log.Printf("%s", err)
		return
	}
	defer sc.Close()

	ackHandler := func(ackedNuid string, err error) {
		if err != nil {
			log.Printf("Error publishing message id %s: %v\n", ackedNuid, err.Error())
		} else {
			log.Printf("Received ACK for message id %s\n", ackedNuid)
		}
	}

	httpHandler := func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Http request url: %s and method: %s \n", r.URL.Path[1:], r.Method)
		if r.Method == "POST" {
			bodyBuffer, _ := ioutil.ReadAll(r.Body)
			// Connect to NATS Streaming server

			sc.PublishAsync(r.URL.Path[1:], bodyBuffer, ackHandler)
			log.Printf("--> %s\n\n", bodyBuffer)
		}
	}

	http.HandleFunc("/", httpHandler)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
